
/*
 
    Thunderboard needs tuning once in place

This Sketch produces a data string as follows (python dictionary)
{'DateTime': YYYYMMDDHHMMSS,
 'Temperature': float yemp,
 'Pressure': float pressure,
 'Humidity': float humidity,
 'LUX': float lux,
 'UVA': float uva,
 'UVB': float uvb,
 'UVIndex': float index,
 'RainAmount': float amount,
 'WindSpeed': float wind speed,
 'WindGust': float wind gust,
 'WindDirection': float wind direction,
 'Lightning': string lightening,
 'LightningDist': int lightening distance,
 'Errors': string errors
}


 */


#include <Wire.h>                              // Wire Library
#include "RTClib.h"                            // Real Time Clock Library
#include <Adafruit_Sensor.h>                   // Required Adafruit Sensor Library
#include <Adafruit_BME280.h>                   // BME Sensor library from Adafruit
#include <Adafruit_TSL2561_U.h>                // LUX Sensor Library from Adafruit
#include <SparkFun_VEML6075_Arduino_Library.h> // UV Sensor Library from Sparkfun
#include <Time.h>                              // Time Library
#include "SDL_Weather_80422.h"                 // Weather Instruments Library
#include "I2C.h"                               // I2C Library
#include "Arduino_AS3935.h"                    // Thunderboard Library

#define intThunder     0  // int 0 for Thunderboard interrupt
#define pinThunder     2  // pin 2 for Thuderboard pin
#define intAnem        4  // int 4 for mega - pin 19   WINDSPEED
#define intRain        5  // int 5 for mega - pin 18   RAIN 
#define pinRain       18  // pin 18 for mega - int 5   RAIN   
#define pinAnem       19  // pin 19 for mega - Int 4   WINDSPEED
#define RTCLED        23  // pin 23 RTC Error LED
#define BMELED        25  // pin 25 Temp/Pressure/Humidty sensor Error LED
#define LUXLED        27  // pin 27 LUX Error LED
#define UVLED         29  // pin 29 UV Sensor Error LED
#define TBCALLED      31  // pin 31 Thunderboard Calibration Error
#define TBNOISELED    33  // pin 33 Thunderboard Noise Floor too high
#define TBDISTURBLED  35  // pin 35 Thunderboard Disturber detected, too many of these may indicate a calibration issue
#define XMITLED       51  // pin 51 Transmit LED
#define POWERLED      53  // pin 53 Power LED

#define SEALEVELPRESSURE_HPA (1013.25)

bool status;                  // used to determine status of BME during setup

float currentWindSpeed;       // Windspeed
float currentWindGust;        // wind gust
float totalRain;              // total rain,  this only calculates total rain for the time during collection, for total will need to add all data points during collection time

String RTCError;              // RTC Error place holder
String BMEError;              // BME Error Place holder
String LUXError;              // Lux sensor Error place holder
String UVError;               // UV Error place holder
String THUNDERError;          // Error for Thunderboard

unsigned long delayTime;            // used to determine how long to wait before next set of values are collected.

volatile int AS3935IrqTriggered;    // Interrupt used to catch lightening strikes


RTC_DS3231 RTC;                                        // RTC
Adafruit_BME280 bme;                                   //  Temp/humid/pressure - I2C - The device's I2C address is either 0x76 or 0x77.
Adafruit_TSL2561_Unified tsl = Adafruit_TSL2561_Unified(TSL2561_ADDR_FLOAT, 12345);    // LUX Sensor - default addess is 0x39
VEML6075 uv;                                           // Create a VEML6075 object - UV Sensor
SDL_Weather_80422 weatherStation(pinAnem, pinRain, intAnem, intRain, A0, SDL_MODE_INTERNAL_AD);   // initialize Weather gauages

//thunderboard
void printAS3935Registers();               // print thunderboard setup info
void AS3935Irq();                          // function for interrupt handler
SDL_Arduino_ThunderBoard_AS3935 AS3935(pinThunder,0x02);    // I2C address  0x020  Initialize thunderboard

// ** function to reset arduino **
// used if power connection is lost to arduino from 5v to sensors
void(* resetFunc) (void) = 0;


void setup() {

  delayTime = 1000;  // time to wait before sending next data sample
  totalRain = 0.0;   // 0 out total rain
  weatherStation.setWindMode(SDL_MODE_SAMPLE, 5.0);

  setPins();                    // function to initialze pins
  Serial.begin(115200);         // start the serial connection
  printHeader();                // function to print header information when arduino starts up, 
  RTCSetup();                   // function to setup and start Real time clock, code in this function can also set the time
  BME280Setup();                // function to setup and start BME sensor - Temp, Humidity, Pressure
  LUXSetup();                   // function to setup and start the LUX - lumens
  UVSetup();                    // function to setup and start the UV sensor
  thunderBoardCalibration();    // function to calibrate the thunderboard
  setupFooter();                // function to print the footer info before gathering data
  
}


void loop() { 
  digitalWrite(XMITLED, HIGH);      // turn on LED when starting to transmit

  Serial.print("{");
  printDateTime();    // get and format Datetime from RTC and print
  printBMEValues();   // print values of temp/humid/pressure sensor
  printLUXValues();   // print values of Light sensor
  printUVValues();    // print values of UV Sensor
  printRainAmount();  // print the total Rain amount
  printWindSpeed();   // print the wind speed and gusts
  printWindDir();     // print wind direction

  //thunderboard
  digitalWrite(TBDISTURBLED, LOW);      // turn off disturber LED
  digitalWrite(TBNOISELED, LOW);        // turn off high noise floor LED

  if(AS3935IrqTriggered)                 // if interrupt is triggered, run this if statement
  {
    // reset the flag
    AS3935IrqTriggered = 0;
    int irqSource = AS3935.interruptSource();           // what was the source that triggered the interrupt
    
    // first step is to find out what caused interrupt
    // returned value irqSource is bitmap field, 
    //        bit 0 - noise level too high, 
    //        bit 2 - disturber detected, 
    //        bit 3 - lightning!
    
    
    if (irqSource & 0b0001)
      printHighNoise();             // call function to handle a high noise floor
      
    if (irqSource & 0b0100)  
      printDisturber();            // call function to handle a disturber, really only needed during testing and setup
      
    if (irqSource & 0b1000)
      printLightning();            // acll function to handle if lightening is detected
    
  
  } else {
    Serial.print("'Lightning': 'none',");
    Serial.print("'LightningDist': 0,");
    
  }


  // these print statements print any sensor errors that occured during data gathering
  // they are added to the end of the data string in one field but are separated by a semicolon.
  Serial.print("'Errors': ");
  Serial.print(RTCError);       // add any RTC errors to serial string
  Serial.print(BMEError);       // add any BME sensor errors to serial string
  Serial.print(LUXError);       // add any lux sensor errors to serial string
  Serial.print(UVError);        // add any UV sensor errors to serial string
  Serial.print("end");          // help determine end of error string if no errors are present
  Serial.println("}");          //  end the serial string with print line, this will allow us to determine breaks between readings
  
  digitalWrite(XMITLED, LOW);   // turn off the transmit LED
  delay(delayTime);             // wait the delay time before getting next set of readings
  
}







///////////////  FUNCTIONS  ///////////////////////////////


//  **  Sets the pinmode and input/output for the pins used  **

void setPins() {
  pinMode(RTCLED, OUTPUT);
  pinMode(BMELED, OUTPUT);
  pinMode(LUXLED, OUTPUT);
  pinMode(UVLED, OUTPUT);
  pinMode(TBCALLED, OUTPUT);
  pinMode(TBNOISELED, OUTPUT);
  pinMode(TBDISTURBLED, OUTPUT);
  pinMode(XMITLED, OUTPUT);
  pinMode(POWERLED, OUTPUT);

  digitalWrite(RTCLED, LOW);
  digitalWrite(BMELED, LOW);
  digitalWrite(LUXLED, LOW);
  digitalWrite(UVLED, LOW);
  digitalWrite(TBCALLED, LOW);
  digitalWrite(TBNOISELED, LOW);
  digitalWrite(TBDISTURBLED, LOW);
  digitalWrite(XMITLED, LOW);
  digitalWrite(POWERLED, HIGH);
  
}

// ** Prints the header information ** //
void printHeader(){
  Serial.println("-----------");
  Serial.println("Fallowfield Weather Station");
  Serial.println("Version 1.0");
  Serial.println("-----------");

  Serial.println("Sensor Test and Setup");
  Serial.println("");
}


// ** Setup and start RTC ** 
void RTCSetup() {
  //RTC
  Serial.println(F("Starting RTC"));   
  if (! RTC.begin()) {
    Serial.println("Problem RTC");   // all is bad 
    digitalWrite(RTCLED, HIGH);      // turing on RTC Error LED
    RTCError = "RTCSTART;";          // adding RTC Error to datastring
   
  } else {
    
    Serial.println("RTC Sensor running");      // all is good
    // following line sets the RTC to the date & time this sketch was compiled
    //Serial.println("setting date and time");
    //RTC.adjust(DateTime(F(__DATE__), F(__TIME__)));
    //Serial.println("Done setting date time");
    Serial.println("");
  }
}


// ** Setup and start the BME sensor **
void BME280Setup() {
  // Temperature / Humidity / Pressure Sensor
  Serial.println("Starting Temperature Sensor"); 
  status = bme.begin();  
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");   // all is bad
    Serial.println("");
    digitalWrite(BMELED, HIGH);           // turn on BME Error LED
    BMEError = "BMESTART;";               // Adding BME Error to Datastring

    
  } else {
    Serial.println("Temp/Humid/Pressure Sensor running");    // All is good
    Serial.println("");
  }
}


// ** setup and start the Lux sensor **
void LUXSetup() {

  // TSL Light Sensor (LUX)
  Serial.println("Starting Light Sensor");

  if(!tsl.begin())
  {
    Serial.print("TSL2561 failed detection... Check your wiring or I2C ADDR!");   // all is bad
    digitalWrite(LUXLED, HIGH);         // turn on LUX Error LED
    LUXError = "LUXSTART;";             // adding LUX error to Datastring
    
  } else {
    
    /* Display some basic information on this sensor */
    displaySensorDetails();                                 // print lux sensor info
  
    /* Setup the sensor gain and integration time */
    configureSensor();                                      // call configure lux sensor function
    Serial.println("LUX Sensor running");                    // all is good
    Serial.println("");
  }
}


// ** setup and start UV Sensor **
void UVSetup() {
  // UV Sensor
  Serial.println("Starting UV Sensor"); 
  if (uv.begin() == false) {
    Serial.println("Unable to communicate with VEML6075.");                 // all is bad
    Serial.println("");
    digitalWrite(UVLED, HIGH);                        // turn on UV Error LED
    UVError = "UV;";                                 // add UV Error to datastring

  } else {
    Serial.println("UV Sensor running");              // all is good
    Serial.println("");
  }
  
}


// ** calibrate thunderboard **
void thunderBoardCalibration() {

  //thunderboard
  Serial.println("Staring Thunderboard");
  I2c.begin();
  I2c.pullup(true);
  I2c.setSpeed(1); //400kHz
  
  // optional control of power for AS3935 via a PNP transistor very useful for lockup prevention and power saving
  // pinMode(4, OUTPUT);
  // digitalWrite(4, LOW);
 
  // reset all internal register values to defaults
  AS3935.reset();
             
  // run calibration, if lightning detector can not tune tank circuit to required tolerance, calibration function will return false
  if(!AS3935.calibrate()) {
    digitalWrite(TBCALLED, HIGH);
    THUNDERError = "THUNDERStart;";          // adding Thunderboard error to datastring
    Serial.println("Tuning out of range, check your wiring and sensor!");   // all is bad
  } else {
    Serial.println(""); 
    Serial.println("Thunderboard running... Starting setup");    // all is good
       
  }

  thunderBoardSetup();          // call function to setup thunderboard
  
}


// ** setup and start thunderboard **
void thunderBoardSetup() {
  
  // first let's turn on disturber indication and print some register values from AS3935
  // tell AS3935 we are indoors, for outdoors use setOutdoors() function
  AS3935.setIndoors();
  //AS3935.setOutdoors();
  
  // turn on indication of distrubers, once you have AS3935 all tuned, you can turn those off with disableDisturbers()
  //AS3935.disableDisturbers();   // disables disturbers
  AS3935.enableDisturbers();
  AS3935.setNoiseFloor(7);      // sets noise floor
  printAS3935Registers();
  AS3935IrqTriggered = 0;     // IRQ used to trigger lightening detection,  using IRQ 0 or pin 2

  attachInterrupt(intThunder,AS3935Irq,RISING);
  
}


// ** print the footer info before taking measurements **
void setupFooter() {
  Serial.println("");
  Serial.println("Setup complete... Start taking measurements!");
  Serial.println();

  
}




// FUNCTIONS IN MAIN LOOP ////////////////////////////////////////////////////////////

// ** format and print the date and time,
void printDateTime() {
  String checkdate = getDate();            // get the date, checking the sensor to be sure it is working
  if (checkdate == "2165165165") {         // if really large date then problem with RTC
    digitalWrite(RTCLED, HIGH);            // turn on RTC Error LED
    RTCError = "RTC;";                     // add RTC Error to datastring
  } else {
    digitalWrite(RTCLED, LOW);             // if all is well, turn off the LED and clear the Error
    RTCError = "";
  }

  Serial.print("'DateTime': ");
  Serial.print(checkdate);                 // get and print the date
  Serial.print(getTime());                 // get and print the time
  Serial.print(",");                       // data separator
 
}


// ** print the values from the BME and some error checking **
void printBMEValues() {
    float c;
    float f;
      
    c = bme.readTemperature();
    f = c * 1.8 + 32;                // convert C to F
    Serial.print("'Temperature': ");
    Serial.print(f);                 // print the temp
    Serial.print(",");               // data seperator

    Serial.print("'Pressure': ");
    Serial.print(bme.readPressure() / 100.0F);     // print pressure
    Serial.print(",");                             // data seperator

    Serial.print("'Humidity': ");
    float humidity = bme.readHumidity();
    Serial.print(humidity);                         // print humidity
    Serial.print(",");                               // data seperator

    if (f < -220 || humidity < 0.01 ) {             // if other errors are detected
      BMEError = "BME;";                            // add BME Error datastring
      digitalWrite(BMELED, HIGH);                   // turn BME Error LED on
    } else {
      BMEError = "";                                // if all is well, clear the error from the datastring
      digitalWrite(BMELED, LOW);                    // turn off BME Error LED
    }

    if (isnan(f)) {
      digitalWrite(BMELED, HIGH);
      Serial.print("RESET ARDUINO");
      BMEError = "BME;";
      resetFunc();
        
    }

}


// ** print the value from the LUX sensor **
void printLUXValues() {
  
    /* Get a new sensor event */ 
    sensors_event_t event;                  // if no error then get a reading
    tsl.getEvent(&event);
 
    /* Display the results (light is measured in lum) */
    if (event.light)                                 
    {
       if (event.light >= 65536) {              // if we get a reading but is greater than 65535, may have issue with sensor
         Serial.print("'LUX': ");
         Serial.print(event.light);             // print value to data string
         Serial.print(",");                     // data seperator
         digitalWrite(LUXLED, HIGH);            // Turn on LUX Error LED if get a reading  
         LUXError = "LUXWIRE;";                 // add Lux error to data string - possible broken I2C connection
                   
       } else {
         Serial.print("'LUX': ");
         Serial.print(event.light);            // print lux reading
         Serial.print(",");                    // data seperator
         digitalWrite(LUXLED, LOW);            // Turn off LUX Error LED if get a reading 
         LUXError = "";                        // clear LUX error from Data string
       }

  
    } else {
      /* If event.light = 0 lux the sensor is probably saturated and no reliable data could be generated! */
      Serial.print("Sensor overload|");    //
      digitalWrite(LUXLED, HIGH);          // Light up LUX Error LED if sensor reads 0
      LUXError = "LUX;";                   // I have gotten this error when it was completely dark
    }

}


// ** print values from UV sensor,  will be UVA, UVB and UV Index **
void printUVValues() {
  
  //Serial.println("UVA, UVB, UV Index");
  float uva = uv.uva();                  // get the UVA reading
  if (uva < 0) {                         // if UVA < 0 then there is an error with the sensor 
    Serial.print("'UVA': ");
    Serial.print(-1);       // all is good print UVA
    Serial.print(",");                    // data seperator
    Serial.print("'UVB': ");
    Serial.print(-1);       // print UVB
    Serial.print(",");                    // data seperator
    Serial.print("'UVIndex': ");
    Serial.print(-1);     // print UV Index
    Serial.print(",");                    // data seperator

    digitalWrite(UVLED, HIGH);           // turn on UV Error LED
    UVError = "UV;";                     // add UV Error to datastring
    
  } else {
    Serial.print("'UVA': ");
    Serial.print(String(uv.uva()));       // all is good print UVA
    Serial.print(",");                    // data seperator
    Serial.print("'UVB': ");
    Serial.print(String(uv.uvb()));       // print UVB
    Serial.print(",");                    // data seperator
    Serial.print("'UVIndex': ");
    Serial.print(String(uv.index()));     // print UV Index
    Serial.print(",");                    // data seperator
    digitalWrite(UVLED, LOW);             // getting readings so turn off UV Error LED
    UVError = "";                         // clear UV Error from data string
  }

  
}



// ** print rain amount **
void printRainAmount() {
  // if want to keep a running track of total rain then this statement will work
  // will need to add a statement to reset the total rain back to 0 or reset Arduino to get back to 0
  //totalRain = totalRain + weatherStation.get_current_rain_total()/25.4;

  // gathers rain amount during time of collection
  totalRain = weatherStation.get_current_rain_total()/25.4;

  Serial.print("'RainAmount': ");
  Serial.print(totalRain);       // print rain amount
  Serial.print(",");             // data seperator

}



// ** print wind speed and wind gust **
void printWindSpeed() {

  //get wind data
  currentWindSpeed = weatherStation.current_wind_speed()/1.6;
  currentWindGust = weatherStation.get_wind_gust()/1.6;

  // print wind speed and gust
  Serial.print("'WindSpeed': ");
  Serial.print(currentWindSpeed);
  Serial.print(",");

  Serial.print("'WindGust': ");
  Serial.print(currentWindGust);
  Serial.print(",");
  
}



// ** print wind direction **
void printWindDir() {

  Serial.print("'WindDirection': ");
  Serial.print(weatherStation.current_wind_direction());
  Serial.print(",");
  
}


// ** High noise floor on the thunderboard
void printHighNoise() {
  digitalWrite(TBNOISELED, HIGH);               // turn on Noise Floor Error LED
  Serial.print("'Lightning': 'HighNoise',");
  Serial.print("'LightningDist': 0,");  
  
}



// ** print distruber detected, this may not be necessary if distrubers is disabled **
void printDisturber() {
  digitalWrite(TBDISTURBLED, HIGH);                 // turn disturber LED ON
  Serial.print("'Lightning': 'Disturber detected',");
  Serial.print("'LightningDist': 0,");

}


// ** print data if lightning is detected **
void printLightning() {
  // need to find how far that lightning stroke, function returns approximate distance in kilometers,
  // where value 1 represents storm in detector's near victinity, and 63 - very distant, out of range stroke
  // everything in between is just distance in kilometers

  int strokeDistance = AS3935.lightningDistanceKm();
  if (strokeDistance == 1) {
    Serial.print("'Lightning': 'Lightning detected',");
    Serial.print("'LightningDist': 1,");
  }
  
  if (strokeDistance == 63) {
    Serial.print("'Lightning': 'Lightning detected',");
    Serial.print("'LightningDist': 63,");
  }
    
  if (strokeDistance < 63 && strokeDistance > 1)  {
    Serial.print("'Lightning': 'Lightning detected',");
    Serial.print("'LightningDist': ");
    Serial.print(strokeDistance,DEC);
    Serial.print(",");
  }

}





// ** print light sensor details - during setup and start of Lux sensor **
void displaySensorDetails(void)
{
  sensor_t sensor;
  tsl.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" lux");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" lux");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" lux");  
  Serial.println("");
  delay(500);
}


// ** configure lux sensor - during setup and start of Lux sensor **
void configureSensor(void)
{
  /* You can also manually set the gain or enable auto-gain support */
  // tsl.setGain(TSL2561_GAIN_1X);      /* No gain ... use in bright light to avoid sensor saturation */
  // tsl.setGain(TSL2561_GAIN_16X);     /* 16x gain ... use in low light to boost sensitivity */
  tsl.enableAutoRange(true);            /* Auto-gain ... switches automatically between 1x and 16x */
  
  /* Changing the integration time gives you better sensor resolution (402ms = 16-bit data) */
  //tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_13MS);      /* fast but low resolution */
  tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_101MS);  /* medium resolution and speed   */
  // tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_402MS);  /* 16-bit data but slowest conversions */

  /* Update these values depending on what you've set above! */  
  Serial.print  ("Gain:         "); Serial.println("Auto");
  Serial.print  ("Timing:       "); Serial.println("101 ms");
  Serial.println("------------------------------------");
}



// ** print Thunderboard setup info during setup and start of Thunderboard **
void printAS3935Registers()
{
  int noiseFloor = AS3935.getNoiseFloor();
  int spikeRejection = AS3935.getSpikeRejection();
  int watchdogThreshold = AS3935.getWatchdogThreshold();
  Serial.print("Noise floor is: ");
  Serial.println(noiseFloor,DEC);
  Serial.print("Spike rejection is: ");
  Serial.println(spikeRejection,DEC);
  Serial.print("Watchdog threshold is: ");
  Serial.println(watchdogThreshold,DEC);  
}

// this is irq handler for AS3935 interrupts, has to return void and take no arguments
// always make code in interrupt handlers fast and short
void AS3935Irq()
{
  AS3935IrqTriggered = 1;
}





// ** Get and Format the date for data string **
String getDate() {
    String myMonth;
    String myDay;
    String myDate;
    
    DateTime now = RTC.now();
        
    myMonth = now.month();
    if (myMonth.length() < 2) {           // RTC will give data in single digits
      myMonth = "0" + myMonth;            // to correct the format must determine the length and add leading 0 
    }                                     // this is to keep the data sting format consistant

    myDay = now.day();                    // RTC will give data in single digits
    if (myDay.length() < 2) {             // to correct the format must determine the length and add leading 0
      myDay = "0" + myDay;                // this is to keep the data sting format consistant
    }

    myDate = now.year() + myMonth + myDay;
    return myDate;
    
}


// ** get and format time for data string **
String getTime() {
    String myHour;
    String myMin;
    String mySec;
    String myTime;
    
    DateTime now = RTC.now();
    
    myHour = now.hour();                         // RTC will give data in single digits
    if (myHour.length() < 2) {                   // to correct the format must determine the length and add leading 0
      myHour = "0" + myHour;                     // this is to keep the data sting format consistant
    }

    myMin = now.minute();                        // RTC will give data in single digits
    if (myMin.length() < 2) {                    // to correct the format must determine the length and add leading 0
      myMin = "0" + myMin;                       // this is to keep the data sting format consistant
    }
    
    mySec = now.second();                        // RTC will give data in single digits
    if (mySec.length() < 2) {                    // to correct the format must determine the length and add leading 0
      mySec = "0" + mySec;                       // this is to keep the data sting format consistant
    }

    myTime = now.hour() + myMin + mySec;
    return myTime;

}
